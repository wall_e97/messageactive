package repository;

import entity.UnitForward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RDocument extends JpaRepository<UnitForward, Long> {
    @Query("SELECT u FROM UnitForward u WHERE u.unit_code = ?1 and u.document_type_code = ?2")
    UnitForward getURLDocument(String unitCode, String documentType);

}

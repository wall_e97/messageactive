package entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "unit_forward_url")
public class UnitForward extends AbstractEntity<Long> {
    @Column(nullable = false, length = 100)
    private String unit_code;

    @Column(nullable = false, length = 1000)
    private String url;

    @Column(nullable = false, length = 1000)
    private String url_auth;

    @Column(nullable = true, length = 100)
    private String document_type_code;

    @Column(nullable = true, length = 100)
    private String method;

    @Column(nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp created_at;

    @Column(nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp updated_at;

    @Column(nullable = true, length = 100)
    private String secret_key;

    @Column(nullable = true, length = 100)
    private String public_key;

    @Column(nullable = true, length = 100)
    private String other_info;

    @Column(nullable = true, length = 100)
    private String pre_process_class;


    public String getUnit_code() {
        return unit_code;
    }

    public void setUnit_code(String unit_code) {
        this.unit_code = unit_code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl_auth() {
        return url_auth;
    }

    public void setUrl_auth(String url_auth) {
        this.url_auth = url_auth;
    }

    public String getDocument_type_code() {
        return document_type_code;
    }

    public void setDocument_type_code(String document_type_code) {
        this.document_type_code = document_type_code;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getPublic_key() {
        return public_key;
    }

    public void setPublic_key(String public_key) {
        this.public_key = public_key;
    }

    public String getOther_info() {
        return other_info;
    }

    public void setOther_info(String other_info) {
        this.other_info = other_info;
    }

    public String getPre_process_class() {
        return pre_process_class;
    }

    public void setPre_process_class(String pre_process_class) {
        this.pre_process_class = pre_process_class;
    }
}

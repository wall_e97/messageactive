package ultil;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HttpClient {
    /**
     * Ham lay lay gia tri cua 1 API theo phuong thuc GET
     * @param apiUrl URL cua API
     * @return Tra ve gia tri dang String
     * @throws IOException Ngoai le khi xay ra loi truy van API
     */
    public static String httpGetStr(String apiUrl) throws IOException {
        final URL url = new URL(apiUrl);
        final HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        final StringBuffer responsedContent = new StringBuffer();
        String strReturn = "";
        try {
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String bufferedLine;
            while ((bufferedLine = bufferedReader.readLine()) != null) {
                responsedContent.append(bufferedLine);
            }
            bufferedReader.close();
            strReturn = responsedContent.toString();
        }
        catch (Exception ex) {
            final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            while ((strReturn = br.readLine()) != null) {
                responsedContent.append(strReturn);
            }
            br.close();
            strReturn = responsedContent.toString();
        }
        return strReturn;
    }

    /**
     * Ham day gia tri cua API theo phuong thuc POST
     * @param apiUrl URL cua API
     * @param body Body cua API
     * @return Tra ve gia tri dang String
     * @throws IOException Ngoai le khi goi API loi
     */
    public static String httpPostStr(String apiUrl, String body) throws IOException {
        final StringBuffer responsedContent = new StringBuffer();
        String strReturn = "";
        final URL url = new URL(apiUrl);
        final HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        try {
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            final OutputStream outStream = conn.getOutputStream();
            outStream.write(body.getBytes());
            outStream.flush();
            outStream.close();
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String bufferedLine;
            while ((bufferedLine = bufferedReader.readLine()) != null) {
                responsedContent.append(bufferedLine);
            }
            bufferedReader.close();
            conn.disconnect();
            strReturn = responsedContent.toString();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            while ((strReturn = br.readLine()) != null) {
                responsedContent.append(strReturn);
            }
            br.close();
            strReturn = responsedContent.toString();
        }
        return strReturn;
    }

    /**
     * Ham lay gia tri cua API theo phuong thuc get
     * @param apiUrl URL cua API
     * @return tra ve dang Object
     * @throws IOException xay ra khi khong ket noi duoc voi API
     */
    public static Object httpGetObj(String apiUrl) throws IOException {
        final URL url = new URL(apiUrl);
        final HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        final StringBuffer responsedContent = new StringBuffer();
        final Gson gson = new GsonBuilder().serializeNulls().create();
        Object objReturn = null;
        try {
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String bufferedLine;
            while ((bufferedLine = bufferedReader.readLine()) != null) {
                responsedContent.append(bufferedLine);
            }
            bufferedReader.close();
            conn.disconnect();
            objReturn = gson.fromJson(responsedContent.toString(), (Class)Object.class);
        }
        catch (Exception ex) {
            final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            String strReturn = "";
            while ((strReturn = br.readLine()) != null) {
                responsedContent.append(strReturn);
            }
            br.close();
            objReturn = gson.fromJson(responsedContent.toString(), (Class)Object.class);
        }
        return objReturn;
    }

    /**
     * Ham goi query API duoi dang POST
     * @param apiUrl URL cua API
     * @param body Body cua API
     * @return Tra ve mot Object
     * @throws IOException xay ra khi khong ket noi duoc API
     */
    public static Object httpPostObj(String apiUrl, String body) throws IOException {
        StringBuffer responsedContent = new StringBuffer();
        String strReturn = "";
        final URL url = new URL(apiUrl);
        final HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        final Gson gson = new GsonBuilder().serializeNulls().create();
        Object objReturn = null;
        try {
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            final OutputStream outStream = conn.getOutputStream();
            outStream.write(body.getBytes());
            outStream.flush();
            outStream.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                responsedContent.append(inputLine);
            }
            in.close();
            conn.disconnect();
            objReturn = gson.fromJson(responsedContent.toString(), (Class)Object.class);
        }
        catch (Exception ex) {
            final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            while ((strReturn = br.readLine()) != null) {
                responsedContent.append(strReturn);
            }
            br.close();
            objReturn = gson.fromJson(responsedContent.toString(), (Class)Object.class);
        }
        return objReturn;
    }

    /**
     * Ham Get API voi tham so truyen vao chua param
     * @param apiUrl URL cua API
     * @param param 1 Collection chua gia tri cua Param
     * @return dua lieu dang Object
     * @throws IOException
     */

    public static Object httpLazyGet(String apiUrl, HashMap<String, String> param) throws IOException {
        StringBuffer urlBuffer = new StringBuffer();
        urlBuffer.append(apiUrl + "?");

        for (Map.Entry<String, String> entry : param.entrySet()) {
            urlBuffer.append(entry.getKey() + "=" + entry.getValue() + "&");
        }

        urlBuffer.deleteCharAt(urlBuffer.length() - 1);
        final URL url = new URL(urlBuffer.toString());
        System.out.println(url);
        final HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        final StringBuffer responsedContent = new StringBuffer();
        final Gson gson = new GsonBuilder().serializeNulls().create();
        Object objReturn = null;
        try {
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String bufferedLine;
            while ((bufferedLine = bufferedReader.readLine()) != null) {
                responsedContent.append(bufferedLine);
            }
            bufferedReader.close();
            conn.disconnect();
            objReturn = gson.fromJson(responsedContent.toString(), (Class)Object.class);
        }
        catch (Exception ex) {
            final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            String strReturn = "";
            while ((strReturn = br.readLine()) != null) {
                responsedContent.append(strReturn);
            }
            br.close();
            objReturn = gson.fromJson(responsedContent.toString(), (Class)Object.class);
        }
        return objReturn;
    }
}

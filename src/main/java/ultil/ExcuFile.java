package ultil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ExcuFile {
    public static void createFile(String name, String Content) throws IOException {
        File tmpFile = File.createTempFile(name, ".txt");
        FileWriter writer = new FileWriter(tmpFile);
        writer.write(Content);
        writer.close();
    }
}

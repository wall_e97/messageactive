package ultil;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonParseUtils {
    private JsonObject jsonObject;

    public JsonParseUtils(final String body) {
        this.jsonObject = new JsonParser().parse(body).getAsJsonObject();
    }

    public String getElementString(final String element) {
        final JsonElement el = this.jsonObject.get(element);
        if (el != null && !el.isJsonNull()) {
            final String partnerCode = this.jsonObject.getAsJsonObject().get(element).getAsString();
            return partnerCode;
        }
        return null;
    }

    public Integer getElementInt(final String element) {
        final JsonElement el = this.jsonObject.get(element);
        if (el != null && !el.isJsonNull()) {
            final Integer partnerCode = this.jsonObject.getAsJsonObject().get(element).getAsInt();
            return partnerCode;
        }
        return 0;
    }
}

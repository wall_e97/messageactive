package message;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import service.SCustomDocument;
import ultil.ExcuFile;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Component
public class Listener {
    @JmsListener(destination = "chimse")
    public void receiveMessage(Message jsonMessage) throws JMSException, IOException {
        SCustomDocument sCustomDocument = new SCustomDocument();
        String messageData = null;
        if(jsonMessage instanceof TextMessage) {
            System.out.println("Nhận tin nhắn: " + jsonMessage);
            TextMessage textMessage = (TextMessage)jsonMessage;
            messageData = textMessage.getText();
            Map map = new Gson().fromJson(messageData, Map.class);

            String from_unit_code = map.get("from_unit_code").toString();
            String document_type = map.get("document_type").toString();
            String description = map.get("description").toString();
            String to_unit_codes = map.get("to_unit_codes").toString();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            ExcuFile.createFile(now.toString(), messageData);


            sCustomDocument.customDocument(unitCode, documentType, messageData);

            System.out.println(messageData.toString());
        }
    }
}

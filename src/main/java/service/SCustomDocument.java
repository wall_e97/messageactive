package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import repository.RDocument;
import ultil.HttpClient;

import java.io.IOException;

@Service
public class SCustomDocument {
    @Autowired
    private SAddressDirectURL sAddressDirectURL;
    public String customDocument(String body, String unitCode, String documentType) throws IOException {

        String strURL = sAddressDirectURL.getURL(unitCode, documentType);
        String result = HttpClient.httpPostStr(strURL, body);
        return result;
    }
}

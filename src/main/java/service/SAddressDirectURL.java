package service;

import entity.UnitForward;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.RDocument;

@Service
public class SAddressDirectURL {
    @Autowired
    private RDocument RDocument;

    /**
     * Ham lay URL can gui di cua message
     * @param unitCode ma don vi truyen vao (thuong la don vi gui)
     * @param documentType loai message
     * @return tra ve URL
     */
    public String getURL(String unitCode, String documentType) {
        UnitForward unit_forward = new UnitForward();
        unit_forward = RDocument.getURLDocument(unitCode, documentType);
        String url = unit_forward.getUrl();
        return url;
    }

    /**
     * Ham lay URL lay token xac thuc. Thuong khong thay doi
     * @param unitCode ma don vi truyen vao (don vi gui)
     * @param documentType loai message
     * @return tra ve URL
     */
    public String getURLAuth(String unitCode, String documentType) {
        UnitForward unit_forward = new UnitForward();
        unit_forward = RDocument.getURLDocument(unitCode, documentType);
        String urlAuth = unit_forward.getUrl_auth();
        return urlAuth;
    }

    /**
     * Ham lay ca dong UnitForwardURL
     * @param unitCode ma don vi truyen vao (don vi gui)
     * @param documentType loai message
     * @return tra ve 1 object
     */
    public UnitForward getUnitForward(String unitCode, String documentType) {
        UnitForward unitForward = new UnitForward();
        unitForward = RDocument.getURLDocument(unitCode, documentType);
        return unitForward;
    }
}
